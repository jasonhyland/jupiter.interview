﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using Jupiter.Interview.Data;
using Jupiter.Interview.Domain;

namespace Jupiter.Interview.FundScraper
{
    public class Scraper
    {
        private readonly IFundRepository _repository;
        private readonly DatabaseContext _context;

        const string JupiterFundPage = "http://webfund6.financialexpress.net/clients/jupiter/pricetable.aspx?Location=GBR&amp;User=PI";

        public Scraper(DatabaseContext context)
        {
            _repository = new FundRepository(context);
            _context = context;
        }

        public IEnumerable<Fund> Scrape()
        {
            LoadNodes();
            return null;
        }

        private void LoadNodes()
        {

            var webHelper = new HtmlWeb();
            var page = webHelper.Load(JupiterFundPage);


            // TODO: Needs to identify non displayed rows in table at some point
            var fundNodes = page.DocumentNode.SelectNodes("//*[@id='OverviewTable']/tbody/tr");

            foreach (HtmlNode fundNode in fundNodes)
            {
                var name = fundNode.ChildNodes[1].InnerText;
                var unitClass = fundNode.ChildNodes[2].InnerText;
                var managers = fundNode.ChildNodes[3].InnerText;
                var objective = fundNode.ChildNodes[4].InnerText;
                var benchmark = fundNode.ChildNodes[5].InnerText;
                var sector = fundNode.ChildNodes[6].InnerText;
                var initialCharge = fundNode.ChildNodes[7].InnerText;
                var annualCharge = fundNode.ChildNodes[8].InnerText;
                var perfFee = fundNode.ChildNodes[9].InnerText;

                var fund = new Fund
                {
                    Name = name,
                    SEDOL = name,
                    FundManagers = GetFundManagers(managers),
                    Objective = GetFundObjective(objective),
                    Sector = sector,
                    Benchmark = benchmark,
                    InitialCharge = GetPercentage(initialCharge),
                    AnnualCharge = GetPercentage(annualCharge),
                    PerformanceFee = GetPercentage(perfFee),
                    UnitClass = GetUnitClass(unitClass)
                };

                _repository.InsertFund(fund);

            }

        }

        private UnitClassType GetUnitClass(string unitClass)
        {
            switch (unitClass.ToLower())
            {
                case "inc":
                    return UnitClassType.Income;
                case "acc":
                    return UnitClassType.Accumilation;
                default:
                    throw new ApplicationException(string.Format("Can't parse unit class : {0}", unitClass));
            }
        }

        private float GetPercentage(string floatValue)
        {
            float value = 0.00f;
            float.TryParse(floatValue, out value);
            return value;
        }

        private ObjectiveType GetFundObjective(string objective)
        {
            var objectiveType = ObjectiveType.None;

            if (objective.ToLower().Contains("growth"))
                objectiveType |= ObjectiveType.Growth;

            if (objective.ToLower().Contains("income"))
                objectiveType |= ObjectiveType.Growth;

            if (objective.ToLower().Contains("multi manager"))
                objectiveType |= ObjectiveType.MultiManager;

            if (objective.ToLower().Contains("absolute return"))
                objectiveType |= ObjectiveType.AbsoluteReturn;

            return objectiveType;
        }

        private ICollection<FundManager> GetFundManagers(string managers)
        {
            var managerList = new List<FundManager>();
            if (managers == "-") return managerList;

            var managerArray = managers.Split(',');

            foreach (string manager in managerArray)
            {
                var fundManager = manager.Trim();
                // Does this already exist
                var selectedManager = _context.FundManagers.FirstOrDefault(fm => fm.Name == fundManager) ??
                        new FundManager { Name = fundManager };
                managerList.Add(selectedManager);
            }

            return managerList;
        }
    }
}
