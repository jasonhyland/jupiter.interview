﻿using System.Collections.Generic;

namespace Jupiter.Interview.Domain
{
    public class FundManager
    {
        public int FundManagerId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Fund> Funds { get; set; }
    }
}