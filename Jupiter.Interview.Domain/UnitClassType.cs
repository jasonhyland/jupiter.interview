﻿namespace Jupiter.Interview.Domain
{
    public enum UnitClassType : int
    {
        Accumilation = 1,
        Income = 2
    }
}