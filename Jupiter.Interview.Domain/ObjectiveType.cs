﻿using System;

namespace Jupiter.Interview.Domain
{
    [Flags]
    public enum ObjectiveType
    {
        None = 0x00,
        Income = 0x01,
        Growth = 0x02,
        AbsoluteReturn = 0x04,
        MultiManager = 0x08
    }
}