﻿using System.Collections.Generic;

namespace Jupiter.Interview.Domain
{
    public class Fund
    {
        private ICollection<FundManager> _fundManagers;

        public Fund()
        {
            _fundManagers = new List<FundManager>();
        }
       
        public int FundId { get; set; }
        public string Name { get; set; }
        public string SEDOL { get; set; }
        public ObjectiveType Objective { get; set; }
        public string Sector { get; set; }
        public float InitialCharge { get; set; }
        public float AnnualCharge { get; set; }
        public float PerformanceFee { get; set; }
        public string Benchmark { get; set; }
        public UnitClassType UnitClass { get; set; }

        public virtual ICollection<FundManager> FundManagers
        {
            get { return _fundManagers; }
            set { _fundManagers = value; }
        }

    }
}