﻿using System;
using System.Data.Entity;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Jupiter.Interview.Data;
using Jupiter.Interview.FundScraper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jupiter.Interview.Tests
{
    [TestClass]
    public class FundScraperTests
    {
        [ClassInitialize]
        public static void SetInitStrategy(TestContext context)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<DatabaseContext>());
        }

        [TestMethod]
        public void CanLoadFundDataFromScraper()
        {
            using (var context = new DatabaseContext())
            {
                var scraper = new Scraper(context);
                scraper.Scrape();
            }

            // Test we have some funds
            using (var context = new DatabaseContext())
            {
                var repo = new FundRepository(context);
                var funds = repo.GetFunds();

                Assert.IsNotNull(funds);
            }
        }
    }
}
