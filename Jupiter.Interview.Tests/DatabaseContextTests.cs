﻿using System;
using System.Data.Entity;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Jupiter.Interview.Data;
using Jupiter.Interview.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jupiter.Interview.Tests
{
    [TestClass]
    public class DatabaseContextTests
    {
        [ClassInitialize]
        public static void SetInitStrategy(TestContext context)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<DatabaseContext>());
        }

        [TestMethod]
        public void CanAddAFund()
        {
            // Arrange
            using (var context = new DatabaseContext())
            {
                // Act
                var fund = new Fund() { SEDOL = "SEDOL1" };
                context.Funds.Add(fund);
                context.SaveChanges();
            }

            // Assert
            using (var context = new DatabaseContext())
            {
                var fund = context.Funds.FirstOrDefault(f => f.SEDOL == "SEDOL1");
                Assert.IsNotNull(fund, "No fund retrieved from DB");
            }
        }

        [TestMethod]
        public void CanAddAFundManager()
        {
            // Arrange
            using (var context = new DatabaseContext())
            {
                var fundManager = new FundManager { Name = "Test Manager" };
                // Act
                context.FundManagers.Add(fundManager);
                context.SaveChanges();
            }

            // Assert
            using (var context = new DatabaseContext())
            {
                var fundManager = context.FundManagers.FirstOrDefault(fm => fm.Name == "Test Manager");
                Assert.IsNotNull(fundManager, "No fundmanager retrieved from DB");
            }
        }

        [TestMethod]
        public void CanAddAFundWithAFundManager()
        {
            // Arrange
            using (var context = new DatabaseContext())
            {

                var fundManager = new FundManager { Name = "Jason Hyland" };
                var fund = new Fund() { SEDOL = "SEDOL2", FundManagers = { fundManager }};
                // Act
                context.Funds.Add(fund);
                context.SaveChanges();
            }

            // Assert
            using (var context = new DatabaseContext())
            {
                var fund = context.Funds.FirstOrDefault(f => f.SEDOL == "SEDOL2");
                Assert.AreEqual(fund.SEDOL, "SEDOL2", "No fundmanager retrieved from DB");
                Assert.AreEqual(fund.FundManagers.First().Name, "Jason Hyland", "No fundmanager retrieved from DB");

            }
        }
    }
}
