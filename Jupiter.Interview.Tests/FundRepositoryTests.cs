﻿using System;
using System.Data.Entity;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Jupiter.Interview.Data;
using Jupiter.Interview.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jupiter.Interview.Tests
{

    [TestClass]
    public class FundRepositoryTests
    {
        [ClassInitialize]
        public static void SetInitStrategy(TestContext context)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<DatabaseContext>());
        }

        [TestMethod]
        public void CanInsertAFund()
        {
            using (var context = new DatabaseContext())
            {
                var fund = new Fund { SEDOL = "FRT0001" };
                var repo = new FundRepository(context);
                repo.InsertFund(fund);
            }

            using (var context = new DatabaseContext())
            {
                var assertFund = context.Funds.FirstOrDefault(f => f.SEDOL == "FRT0001");
                Assert.IsNotNull(assertFund,"Missing Fund");
            }
        }

        [TestMethod]
        public void CanInsertAFundWithFundManagers()
        {
            using (var context = new DatabaseContext())
            {
                
                var fundManager = new FundManager {Name = "Jason Hyland"};
                var fund = new Fund { SEDOL = "FRT0002", FundManagers = { fundManager }};
                var repo = new FundRepository(context);
                repo.InsertFund(fund);
            }

            using (var context = new DatabaseContext())
            {
                var assertFund = context.Funds.FirstOrDefault(f => f.SEDOL == "FRT0002");
                Assert.IsNotNull(assertFund, "Missing Fund");
                Assert.AreEqual("Jason Hyland", assertFund.FundManagers.First().Name);
            }
        }

        [TestMethod]
        public void CanGetListOfAllFunds()
        {
            using (var context = new DatabaseContext())
            {
                var fund1 = new Fund() { SEDOL = "FTR0003A" };
                var fund2 = new Fund() { SEDOL = "FTR0003B" };
                context.Funds.Add(fund1);
                context.Funds.Add(fund2);
                context.SaveChanges();
            }

            using (var context = new DatabaseContext())
            {
                var repo = new FundRepository(context);
                var funds = repo.GetFunds();
                Assert.IsTrue(funds.Count() >= 2);
                // Find the funds
                Assert.IsNotNull(funds.FirstOrDefault(fund => fund.SEDOL == "FTR0003A"),"Couldn't find first fund");
                Assert.IsNotNull(funds.FirstOrDefault(fund => fund.SEDOL == "FTR0003B"),"Couldn't find second fund");

            }
        }

        [TestMethod]
        public void CanGetFundFromFundId()
        {
            int fundId = 0;
            using (var context = new DatabaseContext())
            {
                var fund = new Fund() { SEDOL = "FTR0004" };
                context.Funds.Add(fund);
                context.SaveChanges();
                fundId = fund.FundId;
            }
            
            using (var context = new DatabaseContext())
            {
                var repo = new FundRepository(context);
                var fund = repo.GetFundByID(fundId);
                Assert.IsNotNull(fund,string.Format("Couldn't pull back fund from id {0}",fundId));
            }

        }

        [TestMethod]
        public void CanHandleGetFundByMissingID()
        {
            using (var context = new DatabaseContext())
            {
                var repo = new FundRepository(context);
                var fund = repo.GetFundByID(999999);
                Assert.IsNull(fund);
            }
        }

        [TestMethod]
        public void CanGetFundManagersByFund()
        {
            int fundId = 0;
            using (var context = new DatabaseContext())
            {
                var fundManager1 = new FundManager { Name = "Test Manager 1" };
                var fundManager2 = new FundManager { Name = "Test Manager 2" };
                var fund = new Fund { SEDOL = "FRT0006", FundManagers = { fundManager1, fundManager2 } };
                context.Funds.Add(fund);
                context.SaveChanges();
                fundId = fund.FundId;
            }

            using (var context = new DatabaseContext())
            {
                var repo = new FundRepository(context);
                var managers = repo.GetFundManagersByFund(fundId);

                Assert.IsNotNull(managers, "Missing Fund Managers");
                Assert.IsNotNull(managers.FirstOrDefault(fm => fm.Name == "Test Manager 1"));
                Assert.IsNotNull(managers.FirstOrDefault(fm => fm.Name == "Test Manager 2"));
            }
        }

        [TestMethod]
        public void CanHandleGetFundManagersByMissingFund()
        {
            using (var context = new DatabaseContext())
            {
                var repo = new FundRepository(context);
                var managers = repo.GetFundManagersByFund(9999999);

                Assert.IsNull(managers);
            }
        }
    }
}
