﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jupiter.Interview.Domain;

namespace Jupiter.Interview.Data
{
    public class FundRepository: IFundRepository
    {
        private readonly DatabaseContext _context;

        public FundRepository(DatabaseContext context)
        {
            _context = context;
        }
        
        public IEnumerable<Fund> GetFunds()
        {
            return _context.Funds;
        }

        public Fund GetFundByID(int fundId)
        {
            return _context.Funds.Find(fundId);
        }

        public IEnumerable<FundManager> GetFundManagersByFund(int fundId)
        {
            var fund = GetFundByID(fundId);
            return fund == null ? null : fund.FundManagers;
        }

        public void InsertFund(Fund fund)
        {
            _context.Funds.Add(fund);
            _context.SaveChanges();
        }
    }
}
