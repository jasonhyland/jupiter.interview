﻿using System.Collections.Generic;
using Jupiter.Interview.Domain;

namespace Jupiter.Interview.Data
{
    public interface IFundRepository
    {
        IEnumerable<Fund> GetFunds();
        Fund GetFundByID(int fundId);
        IEnumerable<FundManager> GetFundManagersByFund(int fundId);
        void InsertFund(Fund fund);
    }
}