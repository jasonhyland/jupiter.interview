﻿using System.Data.Entity;
using Jupiter.Interview.Domain;

namespace Jupiter.Interview.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(): base("FundDatabase") { }
        public IDbSet<Fund> Funds { get; set; }
        public IDbSet<FundManager> FundManagers { get; set; }    
    }
}